import Pager from './Pager';
import PropTypes from 'prop-types';
import React from 'react';

class Footer extends React.Component{
    render(){
	return (
	    <div id="footer">
	      <div>
		<strong>{this.props.items}</strong> mail items
	      </div>
	      <div>
		<Pager
		  page={this.props.page}
		  items_per_page={this.props.items_per_page}
		  items={this.props.items}
		  onNext={this.props.onNext}
		  onPrevious={this.props.onPrevious}
		  />
	      </div>
	    </div>
	)
    }
}

Footer.propTypes = {
    items: PropTypes.number.isRequired,
    page: PropTypes.number.isRequired,
    items_per_page: PropTypes.number.isRequired,
    onNext: PropTypes.func.isRequired,
    onPrevious: PropTypes.func.isRequired
}


export default Footer;
