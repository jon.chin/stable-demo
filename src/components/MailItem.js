import CompanyIcon from '../assets/company.svg';
import ForwardIcon from '../assets/forward.svg';
import Moment from 'react-moment';
import ProcessingIcon from '../assets/processing.svg';
import PropTypes from 'prop-types';
import React from 'react';
import RecipientIcon from '../assets/recipient.svg';
import ScanIcon from '../assets/scan.svg';
import ShredIcon from '../assets/shred.svg';

import 'moment-timezone';

class MailItem extends React.Component{
    render(){

	// conditionally render several properties
	let rows = [];
	if(this.props.businessRecipient){
	    rows.push(
		<div key="businessRecipient"
		     className="mail_item_row">
		  <img src={CompanyIcon} alt="company icon" />
		  <span className="mail_item_row_text">
		    {this.props.businessRecipient}
		  </span>
		</div>
	    )
	}
	if(this.props.individualRecipient){
	    rows.push(
		<div key="individualRecipient"
		     className="mail_item_row">
		  <img src={RecipientIcon} alt="recipient icon" />
		  <span className="mail_item_row_text">
		    {this.props.individualRecipient}
		  </span>
		</div>
	    )
	}
	if(this.props.scan){
	    rows.push(
		<div key="scan"
		     className="mail_item_row">
		  <img src={this.props.scan.status === 'processing' ? ProcessingIcon : ScanIcon} alt="scan icon" />
		  <span className="mail_item_row_text">
		    Scan {this.props.scan.status}
		  </span>
		</div>
	    )
	}
	if(this.props.forward){
	    rows.push(
		<div key="forward"
		     className="mail_item_row">
		  <img src={this.props.forward.status === 'processing' ? ProcessingIcon : ForwardIcon} alt="forward icon" />
		  <span className="mail_item_row_text">
		    Forward {this.props.forward.status}
		  </span>
		</div>
	    )
	}
	if(this.props.shred){
	    rows.push(
		<div key="shred"
		     className="mail_item_row">
		  <img src={this.props.shred.status === 'processing' ? ProcessingIcon : ShredIcon} alt="shred icon" />
		  <span className="mail_item_row_text">
		    Shred {this.props.shred.status}
		  </span>
		</div>
	    )
	}
	return(
	    <div className="mail_item_wrapper">
	      <div className="mail_item_photo">
		<img src={this.props.imageUrl} alt="front of mail" />
	      </div>
	      <div className="mail_item_data">
		<h2>
		  {this.props.from}
		</h2>
		{rows.map((row) => {return row;})}
	      </div>
	      <div className="mail_item_date">
		<Moment date={this.props.timestamp}
			format="MMMM D, YYYY"
			/>
		
	      </div>
	    </div>
	)
    }
};

MailItem.propTypes = {
    id: PropTypes.string.isRequired,
    businessRecipient: PropTypes.string,
    from: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    individualRecipient: PropTypes.string,
    timestamp: PropTypes.number.isRequired,
    forward: PropTypes.shape({
	status: PropTypes.oneOf(['completed', 'processing'])
    }),
    scan: PropTypes.shape({
	status: PropTypes.oneOf(['completed', 'processing'])
    }),
    shred: PropTypes.shape({
	status: PropTypes.oneOf(['completed', 'processing'])
    })
}

export default MailItem;
