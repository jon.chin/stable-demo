import Button from './Button';
import PropTypes from 'prop-types';
import React from 'react';


class Pager extends React.Component{
    render(){
	// calculate button disabled logic
	const next_disabled = this.props.items - (this.props.page * this.props.items_per_page) <= 0;
	
	return(
	    <>
		<Button
		    disabled={this.props.page <= 1}
		    onClick={this.props.onPrevious}
		    text="Previous"
		>
		</Button>
		<Button
		disabled={next_disabled}
		    text = "Next"
		    onClick={this.props.onNext}
		/>
	    </>
	);
    }
}

Pager.propTypes = {
    items: PropTypes.number.isRequired,
    page: PropTypes.number.isRequired,
    items_per_page: PropTypes.number.isRequired,
    onNext: PropTypes.func.isRequired,
    onPrevious: PropTypes.func.isRequired
}

export default Pager;
