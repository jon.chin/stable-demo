import PropTypes from 'prop-types';
import React from 'react';

class Button extends React.Component{
    render(){
	return(
	    <button
		disabled={this.props.disabled}
		onClick={this.props.onClick}>
		{this.props.text}
	    </button>
	)
    }
};

Button.defaultProps = {
    disabled: false
};

Button.propTypes = {
    disabled: PropTypes.bool,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

export default Button;
