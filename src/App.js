import data from './data.json';
import Footer from './components/Footer';
import Logo from './assets/stable-logo.svg';
import MailItem from './components/MailItem';
import React from 'react';

import './styles.css';
const items_per_page = 6;

class App extends React.Component{
    constructor(props){
	super(props);
	this.state = {
	    page: 1
	}
    }

    change_page = (change) => {
	// scroll to top in case pager buttons are below the fold
	window.scrollTo(0, 0);
	
	this.setState({
	    page: this.state.page + change
	});
    }
    
    render(){
	// get slice of data to display
	const page_data = data.slice((this.state.page - 1) * items_per_page,
				     this.state.page * items_per_page);
	const mail_items = page_data.map((item) => {
	    return <MailItem
	    id={item.id}
	    businessRecipient={item.businessRecipient}
	    key={item.id}
	    from={item.from}
	    imageUrl={item.imageUrl}
	    individualRecipient={item.individualRecipient}
	    timestamp={item.timestamp}
	    forward={item.forward}
	    scan={item.scan}
	    shred={item.shred}
		/>
	});

	
	return (
	    <>
	      <div id="app_header">
		<img src={Logo} alt="Stable logo" />
	      </div>
	      <div id="app_wrapper">
		<div id="app_info">
		  <h1>
		    All Mail
		  </h1>
		  <p>
		    Here are all of the pieces of mail you’ve received at your Stable address.
		  </p>
		</div>
		<div id="mail_items_wrapper">
		  {mail_items}
		</div>
		<Footer
		  page={this.state.page}
		  items_per_page={items_per_page}
		  items={data.length}
		  onNext={() => {this.change_page(1);}}
		  onPrevious={() => {this.change_page(-1);}}
		  />
	      </div>
	    </>
	)
    }
}

export default App;
